\startcomponent *

\environment env-design
\environment env-mathem
\environment env-layout
\environment env-biblio

\startchapter[title={Algorithms}]

  \startsection
    [
      title={Chern Number},
      reference=sec:chern,
    ]

    The Chern number can be defined for quantum states with at least
    two parameters such as the Brillouin zone in two dimensions.
    Usually the Chern number is defined as
    \placeformula[eq:chern]
    \startformula
      \mathcal C = \frac{1}{2\pi\ii} \sum_{\text{filled bands}} \int_{\tf BZ}
      \diff\bm k\; \mathcal F_{12}(\bm k)
    \stopformula
    where $\mathcal F_{12}(\bm k)$ is the Berry curvature which is
    defined in terms of the Berry connection $\mathcal A_\mu(\bm k)$
    \placeformula
    \startformula
      \mathcal F_{12}(\bm k)
      = \frac{\partial}{\partial k_x} \mathcal A_y(\bm k) - \frac{\partial}{\partial k_y} \mathcal A_x(\bm k)
      \quad\text{with}\quad
      \mathcal A_\mu(\bm k) = \<n(\bm k)|\frac{\partial}{\partial k_\mu}|n(\bm k)>
    \stopformula
    with the normalised Bloch wave functions $\ket{n(k)}$.

    In numerical calculations the Hamiltonian is diagonalised at
    discrete points in the Brillouin zone and the above formula
    becomes impractical because numerical derivatives and integrals can
    only be evaluated with good precision for many lattice points and
    high order of expansion.

    \cite[authornum][Fukui2005] devised an efficient algorithm to
    evaluate the Chern number in a discretised Brillouin zone.  The
    discrete momenta in the Brillouin zone are given by $\bm k_n =
    (k^x_n, k^y_n)$.  Define the ${\tf U}(1)$ link variable from the
    wave functions of the $n$-th band as
    \placeformula
    \startformula
      U_\mu(\bm k_n) = \frac{\<n(\bm k)|n(\bm k + \hat\mu)>}{|\<n(\bm k)|n(\bm k + \hat\mu)>|}
    \stopformula
    where $\hat\mu$ is the lattice displacement vector in
    $\mu$-direction.  From the link variable define the discretised
    Berry curvature
    \placeformula
    \startformula
      F_{12}(\bm k_n) = \ln\biggl( \frac{U_x(\bm k_n) U_y(\bm k_n + \hat x)}{U_x(\bm k_n + \hat y) U_y(\bm k_n)} \biggr)
      \quad\text{such that}\quad
      -\pi < \frac{1}{\ii} F_{12}(\bm k_n) \leq \pi \;.
    \stopformula
    To fulfil this condition we always have to choose the first
    branch of the complex logarithm.  With this the Chern number is
    defined as
    \placeformula
    \startformula
      \mathcal C
      = \frac{1}{2\pi\ii} \sum_{\text{filled bands}} \sum_n F_{12}(\bm k_n) \;.
    \stopformula

    But what if we cannot diagonalise the Hamiltonian in $k$-space?
    This is the case for an arbitrary spin texture.  A real space
    approach to compute the Chern number is needed.
    \cite[authornum][Zhang2013] devised an algorithm were only a
    single exact diagonalization of the Hamiltonian is needed, in
    contrast to \cite[Fukui2005] were the Hamiltonian has to be
    diagonalised for each lattice point in $k$-space.

    Consider a two-dimensional lattice of size $N = L_x \times L_y$ and
    define twisted boundary conditions such that
    \placeformula
    \startformula
      \varphi_\theta^m(x+L_x,y) = \ee^{\ii\theta_x} \varphi_\theta^m(x,y)
      \quad\text{and}\quad
      \varphi_\theta^m(x,y+L_y) = \ee^{\ii\theta_y} \varphi_\theta^m(x,y) \;.
    \stopformula
    For twisted boundary conditions the Chern number is given by
    \placeformula[eq:chern-twisted]
    \startformula
      \mathcal C = \frac{1}{2\pi\ii} \int_{\mathbb T_\theta} \diff\theta
      \<\nabla_\theta \Psi_\theta|\times|\nabla_\theta \Psi_\theta>
    \stopformula
    with $\theta = (\theta_x,\theta_y)$.  The state $\Psi_\theta$
    denotes the many-body wave function which is given in terms of a
    Slater determinant of the single particle wave functions
    $\varphi_\theta^m$ where $m = 1,\ldots,M$ denotes all the {\em
      occupied} states.

    As a next step the single particle wave functions are Fourier
    transformed and one has
    \placeformula
    \startformula
      \varphi_\theta^m(\bm r) = \frac{1}{\sqrt{N}} \sum_{\bm k} \ee^{\ii \bm k \cdot \bm r} \Phi^m(\bm k) \;.
    \stopformula
    The twisted boundary conditions are contained in $\bm k$ which can
    be decomposed such that $\bm k = \bm k^0 + \bm q$ with
    \placeformula
    \startformula
      \bm k^0 = \biggl( \frac{2 \pi x}{L_x}, \frac{2 \pi y}{L_y} \biggr)
      \quad\text{and}\quad
      \bm q = \biggl( \frac{\theta_x}{L_x}, \frac{\theta_y}{L_y} \biggr) \;.
    \stopformula
    This is very convenient because $\bm k^0$ are the momenta for
    conventional periodic boundary conditions.  We perform a change of
    variables in \ineq[eq:chern-twisted] from $\theta$ to $\bm q$.
    This changes the integration domain $\mathbb T_\theta \to R_{\bm
      q} = [0,2\pi/L_x) \times [0,2\pi/L_y)$.
    \placeformula
    \startformula
      \mathcal C = \frac{1}{2\pi\ii} \int_{R_{\bm q}} \diff\bm q
      \<\nabla_{\bm q} \Psi_{\bm q}|\times|\nabla_{\bm q} \Psi_{\bm q}>
    \stopformula
    which is according to Stokes' theorem
    \placeformula
    \startformula
      \mathcal C = \frac{1}{2\pi\ii} \oint_{\partial R_{\bm q}}
      \diff\bm l_{\bm q} \<\Psi_{\bm q}|\nabla_{\bm q} \Psi_{\bm q}>
    \stopformula
    where $\partial R_{\bm q}$ denotes the boundary around the
    rectangle $R_{\bm q}$.  This boundary is discretised into segments
    $\bm q_\alpha$.  The derivatives are replaced by discrete
    differences and the integral is turned into a sum.  Then the Chern
    number is given by
    \placeformula
    \startformula
      \mathcal C = \frac{1}{2\pi} \sum_\alpha \arg[\det(C_{\alpha,\alpha+1})]
    \stopformula
    where $\arg(\cdot)$ is the angle of a complex number and
    $C_{\alpha,\alpha+1}$ are $M\times M$ coupling matrices.  The
    matrix elements are given by
    \placeformula[eq:chern-coupling-matrix]
    \startformula
      C_{\alpha,\alpha+1}^{mn} =
      \<\Phi^m(\bm k^0 + \bm q_\alpha)|\Phi^n(\bm k^0 + \bm q_{\alpha+1})> \;.
    \stopformula
    It is also possible to compute the product of all coupling
    matrices $C_{\alpha,\alpha+1}$ first and then diagonalise the
    resulting matrix.  The sum of the angle of the eigenvalues is
    again proportional to the Chern number.  For sufficiently large
    systems, i.e.\ $L_x, L_y \gg 1$ it suffices to evaluate the
    coupling matrices for four distinct points $q_\alpha$
    \placeformula
    \startformula
      q_0 = (0,0) \;,\quad
      q_2 = \biggl(\frac{2\pi}{L_x},0\biggr) \;,\quad
      q_3 = \biggl(0,\frac{2\pi}{L_y}\biggr) \;,\quad
      q_4 = \biggl(\frac{2\pi}{L_x},\frac{2\pi}{L_y}\biggr) \;.
    \stopformula
    Using the inverse Fourier transformation of
    \ineq[eq:chern-coupling-matrix] we obtain the real space
    expression
    \placeformula
    \startformula
      C_{\alpha,\alpha+1}^{mn} =
      \<\varphi_{\theta=0}^m|\ee^{\ii(\bm q_\alpha - \bm q_{\alpha+1})\bm r}|\varphi_{\theta=0}^n> \;.
    \stopformula
    We diagonalise the product $\tilde C = C_{0,1} C_{1,2} C_{2,3}
    C_{3,0}$ of the coupling matrices to obtain its eigenvalues
    $\lambda_m$ and the Chern number
    \placeformula[eq:chern-real]
    \startformula
      \mathcal C = \frac{1}{2\pi} \sum_m \arg(\lambda_m) \;.
    \stopformula

    To conclude, we test both algorithms against the chiral $p$-wave
    superconductor \ineq[eq:chiral-p-wave] using the BdG form
    \ineq[eq:chiral-p-wave-BdG] to apply the algorithm by
    \cite[authornum][Fukui2005].  The results are shown alongside the
    analytical solution in \in{figure}[fig:chern-comparison].

    \startplacefigure
      [
        location=bottom,
        reference=fig:chern-comparison,
        title={We compare the two different algorithms for the
          computation of the Chern number on a discrete lattice with
          the analytical solution of \ineq[eq:chern-chiral].
          Discontinuous points are shown as unfilled dots.  For better
          visibility the points were shifted off their integer values.
          Both methods show very good agreement for even small lattice
          sizes.  Parameters are 11\texttimes11 sites, $t = 1$,
          $\Delta = 1$.}
      ]
      \externalfigure[figures/chiral-p-wave/chern-number]
    \stopplacefigure

    Both algorithms show very good agreement with the analytical
    solution, which is remarkable considering the small lattices used
    in the picture.  The only difficulties arise at the points where
    the bulk gap closes, i.e.\ $\mu = 0, \pm 4t$.  For $\mu = \pm 4t$
    both algorithms converge to zero rather than the exact one half.
    At $\mu = 0$, however, the $k$-space algorithm does not converge
    due to diverging Berry curvature at two points in the Brillouin
    zone.  This is not really an issue because at these points the
    Chern number is not well defined anyway and the fact that the
    analytical solution yields nice values is a pure coincidence.

  \stopsection

\stopchapter

\stopcomponent
