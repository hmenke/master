CONTEXT = context
TARGETS = p-thesis.pdf

.PHONY: debug final figures thesis

final: MODE = --mode=final
final: thesis

debug: MODE = --mode=debug
debug: thesis

thesis: figures $(TARGETS)

%.pdf: %.mkvi
	$(CONTEXT) $(MODE) $<

figures:
	make -C figures/chiral-p-wave/
	make -C figures/kitaev-chain/
	make -C figures/nadj-perge/
	make -C figures/nakosai/
	make -C figures/phases/
	make -C figures/single-impurity/
	make -C figures/toric-code/
