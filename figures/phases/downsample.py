#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("phase-lambda-4000.dat")
N = data.shape[1]-1

idx = [0]

for col in range(1,N/2):
    if np.any(np.abs(data[:,col]) < 1.2):
        idx.append(col)
        idx.append(N-1-col)

idx.sort()
out = data[::10,idx]

np.savetxt("phase-lambda.dat", out)

N = len(idx)-1
plt.plot(out[:,0],out[:,1:],'b')

plt.plot(out[:,0],out[:,N/2+1],'r')
plt.plot(out[:,0],out[:,N/2+2],'r')

plt.plot(out[:,0],out[:,N/2-2],'g')
plt.plot(out[:,0],out[:,N/2+5],'g')
plt.show()
