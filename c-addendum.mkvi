\startcomponent *

\environment env-design
\environment env-mathem
\environment env-layout
\environment env-biblio


\starttitle[title={Summary}]

  \starttopic[title={English Summary}]

    The main topic of this work was to investigate whether an
    artificial heterostructure between a conventional $s$-wave
    superconductor with strong spin||orbit coupling and a
    non-collinear magnet can host topological phases.

    For topological phases to emerge the gap function has to have odd
    parity.  This is the case for a $p$-wave superconductor.  In
    \in{chapter}[sec:basics] we study two model systems with $p$-wave
    superconductivity to gain some intuition and to introduce tools
    needed later.  \cite[authornum][Kitaev2001] showed that a
    one-dimensional chain can host unpaired Majorana modes at its
    ends.  Because of their non-local character these modes are robust
    against disorder and are therefore promising candidates for
    building blocks in a quantum storage.  Kitaev's chain can be
    promoted to two dimensions, where we find helical Majorana modes
    which propagate around the edges of the system.  Majorana bound
    states at vortices in the superconductor exhibit non-Abelian
    braiding statistics and could thus be used to implement quantum
    gates.

    Intrinsic $p$-wave superconductors are rare in nature.  It is thus
    desirable to engineer an {\em effective} $p$-wave pairing where
    electrons of the same spin are paired together in contrast to
    conventional superconductors, which pair electrons of opposite
    spin.  To introduce $p$-wave pairing magnetic impurities are
    introduced into the conventional superconductor.  Bound states
    form at these impurities and depending on the alignment of the
    classical impurity spins their band structure can exhibit
    non-trivial topology \cite[Choy2011, Nadj-Perge2013,
      Nadj-Perge2014, Pientka2013, Chen2015, Brydon2015].

    In \in{chapter}[sec:magnetic] we study one- and two-dimensional
    systems of magnetic adatoms on a conventional $s$-wave
    superconductor with and without intrinsic spin||orbit coupling.
    In a one-dimensional wire we show that the effects of a helical
    magnetic order can be related {\em exactly} to those of
    Rashba-type spin||orbit coupling along the wire and ferromagnetic
    ordering of the impurity spins.  We then study the interplay of
    the helical spin texture with string spin||orbit coupling and find
    an interesting phase diagram.  By numerical simulation we verify
    our analytical results and show that the topological phase is
    robust against random static disorder.

    For a two-dimensional $s$-wave superconductor and helical spin
    texture there exist Majorana bound states at the edges.  The
    topological phase diagram for the relevant $\mathbb Z_2$ invariant
    can be calculated analytically.  Imposing ferromagnetic order and
    spin||orbit coupling gives rise to a richer phase diagram with
    several distinct topological phases.  Their non-zero Chern number
    indicates the presence of a spontaneous current around the edges
    which was extracted from numerical calculations.  These phases are
    also robust against random static disorder in the spin texture.
    The interplay of the helical spin texture and spin||orbit coupling
    exhibits a rich phase diagram with sometimes high Chern number.

  \stoptopic

  \language[de]

  \starttopic[title={Deutsche Zusammenfassung}]

    Das Hauptthema dieser Arbeit war die Frage, ob eine künstliche
    Heterostruktur zwischen einem konventionellen
    $s$-Wellen-Supraleiter mit starker Spin-Bahn-Kopplung und einem
    nicht kollinearen Magnet topologische Phasen besitzt.

    Damit topologische Phasen auftreten können muss die Energielücke
    ungerade Parität haben.  Dies ist der Fall für einen
    $p$-Wellen-Supraleiter.  In \in{Kapitel}[sec:basics] studieren wir
    zwei Modellsysteme mit $p$-Wellen-Supraleitung um etwas Intuition
    zu erhalten und die später benötigten Werkzeuge kennen zu lernen.
    \cite[authornum][Kitaev2001] zeigte, dass eindimensionale Ketten
    ungepaarte Majoranamoden an den Enden aufweisen können.  Aufgrund
    ihres nichtlokalen Charakters sind diese Moden robust gegenüber
    Unordnung und deshalb vielversprechende Kandidaten für Bausteine
    in einem Quantenspeicher.  Die Kitaev-Kette kann auf zwei
    Dimensionen erweitert werden, wo wir helikale Majoranamoden
    finden, welche entlang der Ränder des Systems propagieren.
    Gebundene Majoranazustände in den Vortizes eines Supraleiters
    weisen nicht-Abelsche Vertauschungsstatistik auf und sind deshalb
    zur Realisierung von Quantengattern geeignet.

    Intrinsische $p$-Wellen-Supraleiter kommen in der Natur selten
    vor.  Es ist daher wünschenswert {\em effektive}
    $p$-Wellen-Supraleitung zu erzeugen, sodass Elektronen des
    gleichen Spins gepaart werden, im Gegensatz zu konventionellen
    Supraleitern, welche Elektronen mit verschiedenem Spin paaren.  Um
    diese Entartung aufzuheben bringt man magnetische Verunreinigungen
    in den konventionellen Supraleiter ein.  An diesen
    Verunreinigungen bilden sich gebundene Zustände, deren
    Bandstruktur abhängig von der Ausrichtung der klassischen Spins
    nicht-triviale Topologie aufzeigt \cite[Choy2011, Nadj-Perge2013,
      Nadj-Perge2014, Pientka2013, Chen2015, Brydon2015].

    In \in{Kapitel}[sec:magnetic] untersuchen wir ein- und
    zweidimensionale Systeme mit magnetischen Adatomen auf einem
    konventionellen $s$-Wellen-Supraleiter mit und ohne intrinsischer
    Spin-Bahn-Kopplung.  An einer eindimensionalen Kette zeigen wir,
    dass die Auswirkungen einer helikalen magnetischen Ordnung deren
    von Rasba-artiger Spin-Bahn-Kopplung und ferromagnetischer Ordnung
    der klassischen Spins {\em exakt} entsprechen.  Wir untersuchen
    außerdem das Wechselspiel der helikalen Spintextur mit starker
    Spin-Bahn-Kopplung und finden ein interessantes Phasendiagramm.
    Wir belegen unsere analytischen Resultate mit numerischen
    Simulationen und zeigen, dass die topologische Phase robust gegen
    zufällige statische Unordnung ist.

    Bei einem zweidimensionalen $s$-Wellen-Supraleiter mit helikaler
    Spintextur existieren gebundene Majoranazustände and den Rändern.
    Das topologische Phasendiagramm der zugehörigen $\mathbb
    Z_2$-Invarianten kann analytisch berechnet werden.
    Ferromagnetische Ordnung und Spin-Bahn-Kopplung führen zu einem
    vielfältigeren Phasesdiagramm mit mehreren unterschiedlichen
    topologischen Phasen.  Deren nichtverschwindende Chernzahl
    weist auf einen spontanen Strom um den Rand herum hin, welcher
    numerisch berechnet wurde.  Diese Phasen sind ebenso robust
    gegenüber zufälliger statischer Unordnung in der Spintextur.  Das
    Wechselspiel der helikalen Spintextur mit Spin-Bahn-Kopplung führt
    zu einem vielfältigen Phasendiagram mit manchmal hoher Chernzahl.

  \stoptopic

\stoptitle

\page
\leavevmode\blank[10*line]

\midaligned{\bf Ehrenwörtliche Erklärung}

\noindent Ich erkläre,
\startitemize[packed]
\item dass ich diese Masterarbeit selbständig verfasst habe,
\item dass ich keine anderen als die angegebenen Quellen benutzt und alle
  wörtlich oder sinngemäß aus anderen Werken übernommenen Aussagen als
  solche gekennzeichnet habe,
\item dass die eingereichte Arbeit weder vollständig noch in wesentlichen
  Teilen Gegenstand eines anderen Prüfungsverfahrens gewesen ist,
\item dass ich die Arbeit weder vollständig noch in Teilen bereits
  veröffentlicht habe, es sei denn, der Prüfungsausschuss hat die
  Veröffentlichung vorher genehmigt
\item und dass der Inhalt des elektronischen Exemplars mit dem des
  Druckexemplars übereinstimmt.
\stopitemize
\blank[3*line]
\noindent
Stuttgart, \documentvariable{metadata:date}\hfill
{\it \documentvariable{metadata:author}}

\stopcomponent
