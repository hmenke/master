\startcomponent *

\environment env-design
\environment env-mathem
\environment env-layout
\environment env-biblio

\starttitle[title={Introduction}]

  Since the successful fabrication of topological insulators in the
  last decade \cite[Koenig2007], tremendous progress has been made in
  understanding and optimizing these non-trivial topological phases of
  matter.  In mathematics, topology is the study of structures which
  are classified according to their invariance under continuous
  deformations.  These mathematical methods can be applied to the
  quantum mechanical wave function, revealing that topologically
  non-trivial states cannot be adiabatically connected to trivial
  ones.

  The best known and also the first reported non-trivial topological
  state is the quantum Hall state, discovered by
  \cite[authornum][Klitzing1980].  The chiral edge states of the
  quantum Hall phase give rise to the quantised Hall conductance
  observed in experiments.  \cite[authornum][Thouless1982] showed that
  this conductance is related to the topological invariant of the
  system, called the Chern number or the TKNN invariant.  A lot of
  effort has been taken to identify these invariant quantities for
  other systems and their interplay with symmetries has led to an
  exhaustive classification for non-interacting
  systems~\cite[Schnyder2008,Kitaev2009,Ryu2010,Chiu2016].

  Topological insulators are gapped in the normal phase and exhibit
  gapless edge modes in the topological phase while the bulk remains
  gapped.  A gapped spectrum is not exclusive to an insulator.  In
  superconductors the BCS ground state is separated from the
  quasiparticle excitation spectrum by the superconducting gap
  \cite[Schnyder2015].  In contrast to topological insulators the
  gapless surface states are superpositions of particle- and hole-like
  excitations.  This is an analogy to particle and anti-particle pairs
  which allows a description in terms of Majorana fermions.
  \cite[authornum][Kitaev2001] showed that the edge modes in a
  one-dimensional $p$-wave superconductor have Majorana character.

  Majorana fermions have been proposed as a platform for quantum
  computation~\cite[Nayak2008].  However, intrinsic topological
  superconductors are rare in nature.  Artificial heterostructures can
  be designed and fabricated in a lab and various proposals for
  proximity induced topological superconductivity exist.  The most
  promising approach is to deposit magnetic adatoms on the surface of
  a conventional $s$-wave
  superconductor~\cite[Choy2011,Nadj-Perge2013,Nadj-Perge2014,Chen2015].
  Magnetic impurities induce low-energy Shiba bound states in the gap
  which hybridise and form bands in the limit of dense impurities.
  Depending on the orientation of the classical impurity spins and
  intrinsic spin||orbit coupling in the superconductor these bands
  exhibit non-trivial topology~\cite[Brydon2015].

  In this thesis we will design an artificial heterostructure between
  a conventional $s$-wave superconductor with strong spin||orbit
  coupling and non-collinear magnets.  We evaluate under which
  conditions Majorana edge states can form and obtain the topological
  phase diagram.  For two-dimensional interfaces we additionally
  extract the spontaneous surface current resulting from the chiral
  edge states.  We investigate these properties using both analytical
  methods as well as numerical tight-binding calculations.
  
\stoptitle

\stopcomponent
